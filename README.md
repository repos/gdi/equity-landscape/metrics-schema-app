# metrics-schema-app

The [Equity Landscape](https://meta.wikimedia.org/wiki/Global_Data_and_Insights_Team/Movement_Data/Equity_Landscape/Introduction) project assembles an initial set of metrics across the domains of readers, editors, community programs, grant making, and affiliates, that capture how people engage with our Movement. These metrics are aligned with economic and social indicators that help further contextualize their meaning in the respective countries and regions.

The web application allows users to interactively explore the schema for each of the metrics presented. The app currently has two chart types: sunburst and icicle. A tabular version, which is also the data source for these charts is available on Meta-Wiki, [on this page](https://meta.wikimedia.org/wiki/Global_Data_and_Insights_Team/Movement_Data/Equity_Landscape/Metrics_Schema_Table).
