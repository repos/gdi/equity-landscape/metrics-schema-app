import pandas as pd
import json

import plotly.graph_objects as go

from dash import dcc, html, dash_table, Dash
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc

application = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
application.title = 'Equity Landscape - Metrics Schema'

def hover_filler(text, label):
    if len(text) - text.count(' ') == 0:
        return label
    else:
        return text    

schema = pd.read_csv('schema.csv', keep_default_na=False)
schema['hover_text'] = schema[['hover_text', 'label']].apply(lambda x: hover_filler(x[0], x[1]), axis=1)

el_description = 'The dashboard assembles an initial set of metrics across the domains of readers, editors, \
                community programs, grant making, and affiliates, that capture how people engage with our Movement. \
                These metrics are aligned with economic and social indicators that help further contextualize their \
                meaning in the respective countries and regions.'
schema_description = 'The following charts will help you to interactively explore the schema for each of the metrics presented.'

navbar = dbc.Navbar(
    dbc.Container(
        [
            html.A(
                [
                    dbc.Row(
                        [
                            dbc.Col(dbc.NavbarBrand('Equity Landscape', style={'font-size': '36px'}))
                        ]
                    ),
                    html.Hr(),
                    dbc.Row(
                        [
                            html.P(el_description, style={'font-size': '14px'}),
                            html.P('Explore the Metrics Schema:', style={'font-size': '18px', 'margin': '0 0 5px 0'}),
                            html.P(schema_description, style={'font-size': '14px'})
                        ]
                    )
                ]
            )
        ]
    )
)

tab_style = {
    'color': '#0c57a8ff',
    'fontWeight': 'bold',
    'font-size': '16px'
}

tab_selected_style = {
    'color': 'black',
    'fontWeight': 'bold',
    'font-size': '16px'
}

sunburst = go.Figure(go.Sunburst(
    labels = schema.label.values.tolist(),
    parents = schema.parent.values.tolist(),
    domain = {'column': 1},
    branchvalues = 'total',
    hoverinfo='text',
    hovertext=schema.hover_text.values.tolist(),
    hoverlabel=dict(namelength=-1)
))
sunburst.update_layout(
    margin=dict(t=0, l=0, r=0, b=0),
    margin_pad=0,
    width=700,
    height=700
)
sunburst_graph = dbc.Row(
    dcc.Graph(figure=sunburst), 
    style={'padding': '2.5% 0 2.5% 25%'}
)

icicle = go.Figure(go.Icicle(
    labels=schema.label.values.tolist(),
    parents=schema.parent.values.tolist(),
    hoverinfo='text',
    hovertext=schema.hover_text.values.tolist(),
    hoverlabel=dict(namelength=-1)
))
icicle.update_layout(
    margin=dict(t=0, l=0, r=0, b=0),
    margin_pad=0,
    width=1000,
    height=700
)
icicle_graph = dbc.Row(
    dcc.Graph(figure=icicle),
    style={'padding': '2.5% 0 2.5% 2.5%'}
)

tabs = dbc.Row([
    dcc.Tabs(id='tab_selection', value='select_sunburst', vertical=True, children=[
        dcc.Tab(
            children=[sunburst_graph],
            label='Sunburst',
            value='select_sunburst',
            style=tab_style, 
            selected_style=tab_selected_style
        ),
        dcc.Tab(
            children=[icicle_graph],
            label='Icicle', 
            value='select_icicle', 
            style=tab_style, 
            selected_style=tab_selected_style
        )
    ])
])

footer = dbc.Row([
    html.Hr(),
    dbc.Col([

            html.A('Metrics Schema on Meta-Wiki', href='https://meta.wikimedia.org/wiki/Global_Data_and_Insights_Team/Movement_Data/Equity_Landscape/Metrics_Schema_Table', target="_blank"),
            html.A(' | '),
            html.A('Source Code', href='https://gitlab.wikimedia.org/repos/gdi/equity-landscape/metrics-schema-app', target="_blank")

    ], style={'text-align': 'right', 'padding': '0 0 1% 0'})
])

application.layout = dbc.Container(
    [
        navbar,
        tabs,
        footer
    ]
)

if __name__ == '__main__':
     application.run()

app = application.server
